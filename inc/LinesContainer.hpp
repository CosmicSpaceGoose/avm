/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   LinesContainer.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/17 15:17:25 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/17 15:17:26 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#pragma once
#ifndef LINESCONTAINER_HPP
# define LINESCONTAINER_HPP
# include "avm.h"

class LinesContainer
{

public:
	LinesContainer( void );
	~LinesContainer( void );
	LinesContainer( const LinesContainer & rhs );

	void				addLine(std::string & line, int l);
	std::map<int, std::string> &	getLines( void );

private:
	std::map<int, std::string> lines;
	LinesContainer &	operator=( const LinesContainer & rhs );

};

#endif /* end of include guard: LINESCONTAINER_HPP */
