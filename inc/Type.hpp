/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Type.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/19 17:02:03 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/19 17:02:04 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#pragma once
#ifndef TYPE_HPP
# define TYPE_HPP

# include "avm.h"
# include "Toolz.hpp"
# include <iomanip>
# include <sstream>

class IOperand;
class Executor;

template <eOperandType E>
class Type : public IOperand
{

public:

	Type( void ) :
		_precision(0),
		_type(Int8),
		_value("-42")
	{}
	~Type( void )
	{}
	Type( Type const & rhs ) :
		_precision(rhs._precision),
		_type(rhs._type),
		_value(rhs._value)
	{}

	Type &	operator=( Type const & rhs ) {
		if (this != &rhs)
		{
			this->_precision = rhs._precision;
			this->_type = rhs._type;
			this->_value = rhs._value;
		}
		return *this;
	}

	Type( std::string const & value ) :
		_precision(static_cast<int>(E)),
		_type(E),
		_value(value)
	{}

	virtual int getPrecision( void ) const {
		return this->_precision;
	}
	eOperandType getType( void ) const {
		return this->_type;
	}

	virtual IOperand const * operator+( IOperand const & rhs ) const {
		return (opTemplate( *this, rhs, '+' ));
	}
	virtual IOperand const * operator-( IOperand const & rhs ) const {
		return (opTemplate( *this, rhs, '-' ));
	}
	virtual IOperand const * operator*( IOperand const & rhs ) const {
		return (opTemplate( *this, rhs, '*' ));
	}
	virtual IOperand const * operator/( IOperand const & rhs ) const {
		return (opTemplate( *this, rhs, '/' ));
	}
	virtual IOperand const * operator%( IOperand const & rhs ) const {
		return (opTemplate( *this, rhs, '%' ));
	}

	virtual std::string const & toString( void ) const {
		return this->_value;
	}

private:
	const int			_precision;
	const eOperandType	_type;
	std::string			_value;
	IOperand const * pseudoFabrique( eOperandType type, std::string const & value ) const {
		if (type < Float) {
			long long val = strtoll(value.c_str(), NULL, 10);
			if ((type == Int8 && val > std::numeric_limits<int8_t>::max())
				|| (type == Int16 && val > std::numeric_limits<int16_t>::max())
				|| (type == Int32 && val > std::numeric_limits<int32_t>::max()))
				throw Toolz::RuntimeException("Value overflow.");
			else if ((type == Int8 && val < std::numeric_limits<int8_t>::min())
				|| (type == Int16 && val < std::numeric_limits<int16_t>::min())
				|| (type == Int32 && val < std::numeric_limits<int32_t>::min()))
				throw Toolz::RuntimeException("Value underflow.");
		}
		else {
			double val = strtod(value.c_str(), NULL);
			if ((type == Float && val > std::numeric_limits< float >::max())
				|| val == HUGE_VAL)
				throw Toolz::RuntimeException("Value overflow.");
			else if ((type == Float && val < std::numeric_limits< float >::lowest())
				|| val == -HUGE_VAL)
				throw Toolz::RuntimeException("Value underflow.");
		}
		if (type == Int8)
			return new Type< Int8 >(value);
		if (type == Int16)
			return new Type< Int16 >(value);
		if (type == Int32)
			return new Type< Int32 >(value);
		if (type == Float)
			return new Type< Float >(value);
		return new Type< Double >(value);
	}

	IOperand const * opTemplate( IOperand const & lhs, IOperand const & rhs, char sign) const {
		double	left = strtod(lhs.toString().c_str(), NULL);
		double	right = strtod(rhs.toString().c_str(), NULL);
		double	result = 0;

		switch (sign) {
			case '+': {
				result = left + right;
				break;
			}
			case '-': {
				result = left - right;
				break;
			}
			case '*': {
				result = left * right;
				break;
			}
			case '/': {
				result = left / right;
				break;
			}
			case '%': {
				result = static_cast<long long>(left) % static_cast<long long>(right);
				break;
			}
		}
		std::stringstream	stream;
		stream << result;
		return (pseudoFabrique( lhs.getType() > rhs.getType() ? lhs.getType() : rhs.getType(), stream.str() ));
	}

};

#endif /* end of include guard: TYPE_HPP */
