/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Executor.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 16:29:32 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/05 16:29:33 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#pragma once
#ifndef EXECUTOR_HPP
# define EXECUTOR_HPP

# include "avm.h"
# include <memory>

class IOperand;
template <eOperandType E> class Type;

class Executor
{
public:

	typedef std::pair< const int, std::vector<std::string> > pairType;
	typedef std::map< std::string, void (Executor::*)( pairType & ) > mapOperatorType;

	Executor( void );
	~Executor( void );
	void		run( InstrType & instr );

	void		opPush( pairType & p );
	void		opPop( pairType & p );
	void		opDump( pairType & p );
	void		opAssert( pairType & p );
	void		opAdd( pairType & p );
	void		opSub( pairType & p );
	void		opMul( pairType & p );
	void		opDiv( pairType & p );
	void		opMod( pairType & p );
	void		opPrint( pairType & p );
	void		opExit( pairType & p );


private:
	int						_exit;
	std::vector< IOperand const * > _stack;
	std::vector<IOperand const * (Executor::*)( std::string const & value ) const> _fabrique;

	void	opTemplate( char sign );

	IOperand const * createOperand( eOperandType type, std::string const & value ) const;
	IOperand const * createInt8( std::string const & value ) const;
	IOperand const * createInt16( std::string const & value ) const;
	IOperand const * createInt32( std::string const & value ) const;
	IOperand const * createFloat( std::string const & value ) const;
	IOperand const * createDouble( std::string const & value ) const;

	Executor( const Executor & rhs );
	Executor &	operator=( const Executor & rhs );

};

#endif /* end of include guard: EXECUTOR_HPP */
