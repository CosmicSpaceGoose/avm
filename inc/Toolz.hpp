/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Toolz.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 13:24:42 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/25 13:24:44 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#pragma once
#ifndef TOOLZ_HPP
# define TOOLZ_HPP
# include "avm.h"

namespace Toolz {

	class LogicalException : public std::logic_error
	{
	public:
		LogicalException(void);
		LogicalException( const std::string & what_arg );
		LogicalException( const char * msg );
	};

	class RuntimeException : public std::runtime_error
	{
	public:
		RuntimeException( void );
		RuntimeException( const std::string & what_arg );
		RuntimeException( const char * what_arg );
	};

	static int	_flags;
	static int	_errors;
	static std::string	c_yellow = "";
	static std::string	c_red = "";
	static std::string	c_half = "";
	static std::string	c_none = "";

	std::string &	fc_yellow( void );
	std::string &	fc_red( void );
	std::string &	fc_half( void );
	std::string &	fc_none( void );
	void		initFlags( char ***av );
	int			getFlag( int flag );
	int			getErrors( void );
	void		logicErrorThrower( std::string msg );
	void		normalMsg( std::string msg );
	void		errorMsg( std::string msg );

};

#endif /* end of include guard: TOOLZ_HPP */
