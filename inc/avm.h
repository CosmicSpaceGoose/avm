/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   avm.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/19 13:45:29 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/19 13:45:40 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#pragma once
#ifndef AVM_H
# define AVM_H

# define CLRS	1
# define HELP	2
# define PRNT	4
# define SOFT	8
# define VERB	16

# include <string>
# include <iostream>
# include <vector>
# include <map>
# include <cmath>

typedef std::map<int, std::vector<std::string> > InstrType;

enum eOperandType {
	Int8,
	Int16,
	Int32,
	Float,
	Double
};

#endif /* end of include guard: AVM_H */
