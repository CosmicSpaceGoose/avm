/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionsContainer.hpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/17 15:17:27 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/17 15:17:28 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSTRUCTIONSCONTAINER_HPP
# define INSTRUCTIONSCONTAINER_HPP

# include "avm.h"

class InstructionsContainer
{

public:
	InstructionsContainer( void );
	InstructionsContainer( InstrType & instr );
	~InstructionsContainer( void );

	InstrType	getInstructions( void ) const ;

private:
	InstrType * _instructions;
	InstructionsContainer( const InstructionsContainer & rhs );
	InstructionsContainer &	operator=( const InstructionsContainer & rhs );

};

#endif /* end of include guard: INSTRUCTIONSCONTAINER_HPP */
