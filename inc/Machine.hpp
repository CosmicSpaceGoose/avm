/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Machine.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 16:58:02 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/05 16:58:04 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#pragma once
#ifndef MACHINE_HPP
# define MACHINE_HPP

# include <fstream>
# include <regex>
# include <cstring>
# include <stdexcept>
# include "avm.h"

class LinesContainer;
class InstructionsContainer;
class Executor;

class Machine
{

public:

	Machine(void);
	~Machine(void);

	void		init( char *** av );
	void		readFromInput( void );
	void		readFromFiles( char * files );
	void		compile( void );
	int			run( void );

private:

	Machine &		operator=( const Machine & rhs );
	Machine( const Machine & rhs );

	InstructionsContainer * _progr;
	LinesContainer *	_lines;

	void		parser( InstrType & in );
	InstrType	lexer( const std::map<int, std::string> &in );


};

#endif /* end of include guard: MACHINE_HPP */
