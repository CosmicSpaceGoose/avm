/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Machine.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 16:58:00 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/05 16:58:02 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Machine.hpp"
#include "LinesContainer.hpp"
#include "InstructionsContainer.hpp"
#include "Executor.hpp"
#include "Toolz.hpp"

/* ********************************** MACHINE ******************************* */

	Machine::Machine(void) : _progr(nullptr), _lines(nullptr)
	{}
	Machine::~Machine(void) {
		delete this->_progr;
		delete this->_lines;
	}
	Machine::Machine( const Machine & rhs ) {
		*this = rhs;
	}
	Machine &		Machine::operator=( const Machine & rhs ) {
		(void)rhs;
		return *this;
	}

/* ********************************** READERS ******************************* */

	void	Machine::readFromInput( void ) {
		LinesContainer v;
		std::string str;
		std::string sub;
		int		l = 1;

		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "avm: read from input.." );
		while (std::getline(std::cin, str))
		{
			if (std::regex_match(str, std::regex("(.*);;(.*)"))) {
				sub = str.substr(0, str.find(";"));
				if (!sub.empty())
					v.addLine(sub, l);
				break;
			}
			sub = str.substr(0, str.find(";"));
			if (!sub.empty())
				v.addLine(sub, l);
			l++;
		}
		this->_lines = new LinesContainer(v);
	}

	void	Machine::readFromFiles( char * file ) {
		LinesContainer v;
		std::string str;
		std::string sub;
		int		l;

		if (Toolz::getFlag(VERB))
			std::cout << Toolz::fc_half() << "avm: read from file " << Toolz::fc_none() << file << std::endl;
		std::ifstream fd(file);
		if (!fd)
		{
			throw Toolz::RuntimeException(Toolz::fc_red() + "avm fatal error: "
			+ Toolz::fc_none() + "can't open file " + file);
		}
		l = 1;
		while (!fd.eof())
		{
			getline(fd, str);
			sub = str.substr(0, str.find(";"));
			if (!sub.empty())
				v.addLine(sub, l);
			l++;
		}
		fd.close();
		this->_lines = new LinesContainer(v);
	}

/* *********************************** STAGES ******************************* */


	void	Machine::init( char ***av ) {
		try {
			Toolz::initFlags(av);
		}
		catch ( std::exception & e ) {
			std::cerr << e.what() << std::endl;
			std::cerr << "usage: ./abs [-cdhpsv] [filename ...]" << std::endl;
			std::cerr << "\t-c color output" << std::endl;
			std::cerr << "\t-h help" << std::endl;
			std::cerr << "\t-p print instructions instead of execute" << std::endl;
			std::cerr << "\t-s soft handle of errors" << std::endl;
			std::cerr << "\t-v verbose VM stages and operations" << std::endl;
			exit(1);
		}
	}

	void	Machine::compile( void ) {
		try
		{
			InstrType ptr;

			ptr = lexer(this->_lines->getLines());
			parser(ptr);
			this->_progr = new InstructionsContainer(ptr);
			if (Toolz::getErrors() > 2)
			{
				throw Toolz::RuntimeException(Toolz::fc_red() + "avm fatal error: "
				+ Toolz::fc_none() + "too many lexical errors were occured.");
			}
		}
		catch ( std::exception & e )
		{
			std::cerr << e.what() << std::endl;
			exit(1);
		}
	}

	int		Machine::run(void) {
		InstrType instr = Machine::_progr->getInstructions();

		if (Toolz::getFlag(PRNT)) {
			if (Toolz::getFlag(VERB))
				Toolz::normalMsg( "avm: printing.." );
			for (auto iter = instr.begin(); iter != instr.end(); iter++)
			{
				for (auto it = iter->second.begin(); it != iter->second.end(); it++) {
					std::cout << *it << "\t";
				}
				std::cout << std::endl;
			}
			return (0);
		}
		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( std::string("avm: running.."));
		try {
			Executor	instance;
			instance.run(instr);
		}
		catch ( std::exception & e ) {
			std::cerr << Toolz::fc_red() << "avm fatal error: " << Toolz::fc_none() << e.what() << std::endl;
			return (1);
		}
		return (0);
	}

/* ******************************** LEXER/PARSER **************************** */

	InstrType	Machine::lexer( const std::map<int, std::string> &in ) {
		InstrType	out;
		int			i;

		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "avm: parsing.." );
		for (auto it = in.begin(); it != in.end(); it++)
		{
			std::vector<std::string>	p;
			char	*elem;

			elem = strtok(const_cast<char *>(it->second.c_str()), " ()\t");
			p.push_back(std::string(elem));
			i = 0;
			while ((elem = strtok(NULL, " ()\t")))
			{
				if (i > 1)
				{
					if (Toolz::getFlag(SOFT))
						std::cerr << Toolz::fc_yellow() << "alarma! " << Toolz::fc_none() <<\
			"extra symbols were ignored at line " << it->first << "." << std::endl;
					else
						throw Toolz::LogicalException(Toolz::fc_red() + "logic error:"
		+ Toolz::fc_none() + " extra symbols found at line " + std::to_string(it->first) + ". abort.");
					break;
				}
				p.push_back(std::string(elem));
				i++;
			}
			out[it->first] = p;
			p.clear();
		}
		return (out);
	}

	void		Machine::parser( InstrType & in ) {
		for (auto it = in.begin(); it != in.end(); it++)
		{
			std::string		line(std::to_string(it->first));
			auto it2 = it->second.begin();

			if (!std::regex_match(*it2, std::regex("push|pop|dump|assert|add|sub|mul|div|mod|print|exit")))
			{
				Toolz::logicErrorThrower("unknown instruction at line " + line + ": " + *it2);
				in.erase(it);
				it--;
			}
			else if (std::regex_match(*it2, std::regex("push|assert")))
			{
				if (++it2 == it->second.end())
				{
					Toolz::logicErrorThrower("missed operand at line " + line);
					in.erase(it);
					it--;
					continue;
				}
				if (std::regex_match(*it2, std::regex("int8|int16|int32")))
				{
					if (++it2 == it->second.end())
					{
						Toolz::logicErrorThrower("missed operand value at line " + line);
						in.erase(it);
						it--;
						continue;
					}
					if (!std::regex_match(*it2, std::regex("[-]?[\\d]+")))
						Toolz::logicErrorThrower("invalid operand value at line " + line + ": " + *it2);
				}
				else if (std::regex_match(*it2, std::regex("float|double")))
				{
					if (++it2 == it->second.end())
					{
						Toolz::logicErrorThrower("missed operand value at line " + line);
						in.erase(it);
						it--;
						continue;
					}
					if (!std::regex_match(*it2, std::regex("[-]?[\\d]+.[\\d]+")))
						Toolz::logicErrorThrower("invalid operand value at line " + line + ": " + *it2);
				}
				else
				{
					Toolz::logicErrorThrower("unknown operand at line " + line + ": " + *it2);
					in.erase(it);
					it--;
				}
			}
			else
				if (++it2 != it->second.end())
					Toolz::logicErrorThrower("instruction doesn't use operands at line " + line);
		}
	}
