/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/03 18:29:33 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/03 18:29:35 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Machine.hpp"
#include <exception>

/*
**      v.25-07-18
*/

int		main(int ac, char *av[])
{
	Machine	avm;

	ac--;
	avm.init(&(++av));
	try {
		if (!*av)
			avm.readFromInput();
		else
			avm.readFromFiles(*av);
	}
	catch ( std::exception & e ) {
		std::cerr << e.what() << std::endl;
		exit(1);
	}
	avm.compile();
	return (avm.run());
}
