/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Executor.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 16:29:21 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/05 16:29:23 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Executor.hpp"
#include "IOperand.hpp"
#include "Type.hpp"
#include "Toolz.hpp"
#include <iostream>
#include <limits>
#include <unistd.h>

/* ********************************* Executor ******************************* */

	Executor::Executor( void ) {
		_fabrique.push_back(&Executor::createInt8);
		_fabrique.push_back(&Executor::createInt16);
		_fabrique.push_back(&Executor::createInt32);
		_fabrique.push_back(&Executor::createFloat);
		_fabrique.push_back(&Executor::createDouble);
		_exit = 0;
	}
	Executor::~Executor( void ) {
		for (auto i = _stack.begin(); i != _stack.end(); ++i) {
			delete *i;
		}
		_stack.clear();
	}

	Executor::Executor( const Executor & rhs ) {
		(void)rhs;
	}
	Executor &	Executor::operator=( const Executor & rhs ) {
		(void)rhs;
		return *this;
	}

/* ********************************* Fabrique ******************************* */

	IOperand const * Executor::createOperand( eOperandType type, std::string const & value ) const {
		if (type < Float) {
			long long val = strtoll(value.c_str(), NULL, 10);
			if ((type == Int8 && val > std::numeric_limits<int8_t>::max())
				|| (type == Int16 && val > std::numeric_limits<int16_t>::max())
				|| (type == Int32 && val > std::numeric_limits<int32_t>::max()))
				throw Toolz::RuntimeException("Value overflow.");
			else if ((type == Int8 && val < std::numeric_limits<int8_t>::min())
				|| (type == Int16 && val < std::numeric_limits<int16_t>::min())
				|| (type == Int32 && val < std::numeric_limits<int32_t>::min()))
				throw Toolz::RuntimeException("Value underflow.");
		}
		else {
			double val = strtod(value.c_str(), NULL);
			if ((type == Float && val > std::numeric_limits< float >::max())
				|| val == HUGE_VAL)
				throw Toolz::RuntimeException("Value overflow.");
			else if ((type == Float && val < std::numeric_limits< float >::lowest())
				|| val == -HUGE_VAL)
				throw Toolz::RuntimeException("Value underflow.");
		}
		return (this->*(_fabrique[static_cast<int>(type)]))(value);
	}

	IOperand const * Executor::createInt8( std::string const & value ) const {
		return new Type<Int8>( value );
	}
	IOperand const * Executor::createInt16( std::string const & value ) const {
		return new Type<Int16>( value );
	}
	IOperand const * Executor::createInt32( std::string const & value ) const {
		return new Type<Int32>( value );
	}
	IOperand const * Executor::createFloat( std::string const & value ) const {
		return new Type<Float>( value );
	}
	IOperand const * Executor::createDouble( std::string const & value ) const {
		return new Type<Double>( value );
	}

/* ******************************** Operations ****************************** */

	void		Executor::opTemplate( char sign ) {
		if (this->_stack.size() < 2)
			throw Toolz::RuntimeException("Not anough (need 2) operands in stack.");
		std::unique_ptr<const IOperand> rhs(this->_stack.back());
		this->_stack.pop_back();
		std::unique_ptr<const IOperand> lhs(this->_stack.back());
		this->_stack.pop_back();
		switch (sign) {
			case '+': {
				this->_stack.push_back( *lhs + *rhs );
				break;
			}
			case '-': {
				this->_stack.push_back( *lhs - *rhs );
				break;
			}
			case '*': {
				this->_stack.push_back( *lhs * *rhs );
				break;
			}
			case '/': {
				if ((*rhs).toString() == "0" || (*rhs).toString() == "0.0")
					throw Toolz::RuntimeException("Division by zero.");
				this->_stack.push_back( *lhs / *rhs );
				break;
			}
			case '%': {
				if ((*rhs).toString() == "0" || (*rhs).toString() == "0.0")
					throw Toolz::RuntimeException("Remainder by zero.");
				this->_stack.push_back( *lhs % *rhs );
				break;
			}
		}
	}

	void		Executor::opPush( pairType & p ) {
		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "--push operand" );
		auto it = p.second.begin();
		eOperandType	type = Int8;
		if (*(it + 1) == "int16")
			type = Int16;
		else if (*(it + 1) == "int32")
			type = Int32;
		else if (*(it + 1) == "float")
			type = Float;
		else if (*(it + 1) == "double")
			type = Double;
		this->_stack.push_back(Executor::createOperand(type, *(it + 2)));
	}
	void		Executor::opPop( pairType & p ) {
		(void)p;
		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "--pop from stack" );
		if (!(this->_stack.empty())) {
			delete this->_stack.back();
			this->_stack.pop_back();
		}
		else
			throw Toolz::RuntimeException("Can't pop, stack is empty.");
	}
	void		Executor::opDump( pairType & p ) {
		(void)p;
		std::vector< std::string > v;
		v.push_back("Int8");
		v.push_back("Int16");
		v.push_back("Int32");
		v.push_back("Float");
		v.push_back("Double");
		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "--dump stack" );
		for (auto i = this->_stack.end() - 1; i != this->_stack.begin() - 1; i--) {
			std::cout << "[" << v[(*i)->getType()] << "] " << (*i)->toString() << std::endl;
		}
		v.clear();
	}
	void		Executor::opAssert( pairType & p ) {
		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "--assert operand" );
		auto it = p.second.begin();
		eOperandType	type = Int8;

		if (*(it + 1) == "int16")
			type = Int16;
		else if (*(it + 1) == "int32")
			type = Int32;
		else if (*(it + 1) == "float")
			type = Float;
		else if (*(it + 1) == "double")
			type = Double;
		if (this->_stack.empty()
			|| this->_stack.back()->getType() != type
			|| this->_stack.back()->toString() != *(it + 2))
			throw Toolz::RuntimeException("Assert is false.");
	}
	void		Executor::opAdd( pairType & p ) {
		(void)p;
		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "--add operands" );
		Executor::opTemplate('+');
	}
	void		Executor::opSub( pairType & p ) {
		(void)p;
		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "--substruct operands" );
		Executor::opTemplate('-');
	}
	void		Executor::opMul( pairType & p ) {
		(void)p;
		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "--multiply operands" );
		Executor::opTemplate('*');
	}
	void		Executor::opDiv( pairType & p ) {
		(void)p;
		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "--division operands" );
		Executor::opTemplate('/');
	}
	void		Executor::opMod( pairType & p ) {
		(void)p;
		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "--modulo operands" );
		Executor::opTemplate('%');
	}
	void		Executor::opPrint( pairType & p ) {
		(void)p;
		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "--print operand" );
		if (this->_stack.size() == 0)
			throw Toolz::RuntimeException("Can't print, stack is empty.");
		if ( this->_stack.back()->getType() != Int8 )
			throw Toolz::RuntimeException("Can't print, invalid type.");
		std::cout << this->_stack.back()->toString() << std::endl;
	}
	void		Executor::opExit( pairType & p ) {
		(void)p;
		if (Toolz::getFlag(VERB))
			Toolz::normalMsg( "--exit program" );
		this->_exit = 1;
	}

	void		Executor::run( InstrType & instr )
	{
		mapOperatorType	operations;

		operations["push"] = &Executor::opPush;
		operations["pop"] = &Executor::opPop;
		operations["dump"] = &Executor::opDump;
		operations["assert"] = &Executor::opAssert;
		operations["add"] = &Executor::opAdd;
		operations["sub"] = &Executor::opSub;
		operations["mul"] = &Executor::opMul;
		operations["div"] = &Executor::opDiv;
		operations["mod"] = &Executor::opMod;
		operations["print"] = &Executor::opPrint;
		operations["exit"] = &Executor::opExit;

		for (auto i = instr.begin(); i != instr.end() && !_exit; i++) {
			(this->*(operations[i->second[0]]))( *i );
		}
		if (!_exit)
			throw Toolz::RuntimeException("Program doesn’t have an exit instruction.");
		operations.clear();
	}
