/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   LinesContainer.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/17 15:17:05 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/17 15:17:06 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "LinesContainer.hpp"
#include <unistd.h>
	LinesContainer::LinesContainer( void )
	{}
	LinesContainer::~LinesContainer( void ) {
		this->lines.clear();
	}

	LinesContainer::LinesContainer( const LinesContainer & rhs ) : lines(rhs.lines)
	{}

	LinesContainer &	LinesContainer::operator=( const LinesContainer & rhs ) {
		if (this != &rhs)
		{
			this->lines = rhs.lines;
		}
		return (*this);
	}

	void				LinesContainer::addLine(std::string & line, int l) {
		this->lines[l] = line;
	}

	std::map<int, std::string> &	LinesContainer::getLines( void ) {
		return this->lines;
	}
