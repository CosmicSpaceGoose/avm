/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Toolz.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 13:22:27 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/25 13:22:28 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Toolz.hpp"
#include <regex>
#include <unistd.h>

/* ***************************** LogicalException *************************** */

	Toolz::LogicalException::LogicalException( void ) : std::logic_error("logic error has occured.")
	{}
	Toolz::LogicalException::LogicalException( const std::string & what_arg ) : std::logic_error(what_arg.c_str())
	{}
	Toolz::LogicalException::LogicalException( const char * what_arg ) : std::logic_error(what_arg)
	{}

/* ***************************** RuntimeException *************************** */

	Toolz::RuntimeException::RuntimeException( void ) : std::runtime_error("runtime error has occured.")
	{}
	Toolz::RuntimeException::RuntimeException( const std::string & what_arg ) : std::runtime_error(what_arg.c_str())
	{}
	Toolz::RuntimeException::RuntimeException( const char * what_arg ) : std::runtime_error(what_arg)
	{}

/* *********************************** Toolz ******************************** */

	void		Toolz::initFlags(char ***av) {
		while (**av && ***av == '-')
		{
			if (!std::regex_match(**av, std::regex("^(-)[chpsv]+$"))
			&&	strcmp(**av, "--")) {
				throw Toolz::RuntimeException("runtime error: illegal option.");
			}
			std::string str(**av);
			for (size_t i = 1; i < str.length(); i++)
			{
				switch (str[i]) {
					case 'c':
						Toolz::_flags |= CLRS;
						break;
					case 'h':
						Toolz::_flags |= HELP;									//???
						break;
					case 'p':
						Toolz::_flags |= PRNT;
						break;
					case 's':
						Toolz::_flags |= SOFT;
						break;
					case 'v':
						Toolz::_flags |= VERB;
						break;
				}
			}
			(*av)++;
		}
		if (isatty(0) && isatty(1) && isatty(2) && Toolz::_flags & CLRS)
		{
			Toolz::c_yellow = "\e[33m";
			Toolz::c_red = "\e[31m";
			Toolz::c_half = "\e[2;3;m";
			Toolz::c_none = "\e[0m";
		}
	}


	void		Toolz::logicErrorThrower( std::string msg ) {
		if (Toolz::_flags & SOFT)
		{
			std::cerr << c_yellow << "alarma! " << c_none << msg << "." << std::endl;
			Toolz::_errors++;
		}
		else
			throw Toolz::LogicalException(c_red + "logic error: " + c_none + msg + ". abort.");
	}

	void		Toolz::normalMsg( std::string msg ) {
		std::cout << Toolz::c_half << msg << Toolz::c_none << std::endl;
	}

	void		Toolz::errorMsg( std::string msg ) {
		std::cerr << msg << std::endl;
	}

	int			Toolz::getFlag( int flag ) {
		return (Toolz::_flags & flag);
	}

	int			Toolz::getErrors( void ) {
		return (Toolz::_errors);
	}

	std::string &	Toolz::fc_yellow( void ) {
		return Toolz::c_yellow;
	}
	std::string &	Toolz::fc_red( void ) {
		return Toolz::c_red;
	}
	std::string &	Toolz::fc_half( void ) {
		return Toolz::c_half;
	}
	std::string &	Toolz::fc_none( void ) {
		return Toolz::c_none;
	}
