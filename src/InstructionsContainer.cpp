/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionsContainer.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/17 15:17:07 by dlinkin           #+#    #+#             */
/*   Updated: 2018/07/17 15:17:09 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InstructionsContainer.hpp"
#include <unistd.h>
	InstructionsContainer::InstructionsContainer( void )
	{
		// write(1, "IC: +\n", 6);
	}
	InstructionsContainer::InstructionsContainer( InstrType & instr )
	{
		// write(1, "IC: c\n", 6);
		this->_instructions = new InstrType(instr);
	}

	InstrType	InstructionsContainer::getInstructions( void ) const
	{
		return this->_instructions[0];
	}

	InstructionsContainer::~InstructionsContainer( void )
	{
		// write(1, "IC: -\n", 6);
		delete this->_instructions;
	}

	InstructionsContainer::InstructionsContainer( const InstructionsContainer & rhs ) : _instructions(rhs._instructions)
	{}
	InstructionsContainer &	InstructionsContainer::operator=( const InstructionsContainer & rhs )
	{
		if (this != &rhs)
		{
			this->_instructions = rhs._instructions;
		}
		return (*this);
	}
