# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/05/22 12:27:21 by dlinkin           #+#    #+#              #
#    Updated: 2018/05/22 12:27:23 by dlinkin          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CXX = g++
CXXFLAGS = -Wall -Wextra -Werror -std=c++11 $(INC)
SRC_DIR = src
SRC =	main.cpp\
		Machine.cpp\
		LinesContainer.cpp\
		InstructionsContainer.cpp\
		Executor.cpp\
		Toolz.cpp

SRC_FILES = $(foreach a,$(SRC), $(foreach b,$(SRC_DIR), $b/$a))
OBJ_DIR = obj
OBJ_FILES = $(foreach a,$(SRC:%.cpp=%.o), $(foreach b,$(OBJ_DIR), $b/$a))
INC_DIR = inc
INC = -I./inc
BIN = avm
HED =	inc/avm.h\
		inc/Type.hpp\
		inc/IOperand.hpp

CYAN = \x1b[1;36m
NON = \x1b[0m
CYANN = \x1b[36m
GREEN = \x1b[32m

RUN = -vsc
PRINT = -vscp
TEST_FILES =	cd/add32to8.avm\
				cd/add8to16.avm\
				cd/add8tofloat.avm\
				cd/adddtof.avm\
				cd/atata.avm\
				cd/badinstr.avm\
				cd/divzero.avm\
				cd/double.avm\
				cd/empty.avm\
				cd/example.avm\
				cd/exit.avm\
				cd/extrasym.avm\
				cd/infnan.avm\
				cd/modzero.avm\
				cd/mult32\&f.avm\
				cd/multf\&f.avm\
				cd/noexit.avm\
				cd/oflowmult.avm\
				cd/o+uflow.avm\
				cd/print.avm\
				cd/toobadinstr.avm\
				cd/values.avm

STRING1 = $(CYAN)---Compile_$(BIN)$(NON)
STRING2 = $(CYAN)---Remove_$(BIN)_O_Files$(NON)
STRING3 = $(CYAN)---Remove_$(BIN)$(NON)
STRING4 = $(CYAN)---Running$(NON)
STRING5 = $(CYAN)---Copy binary file in ~/my_bin$(NON)

all: $(OBJ_DIR) $(BIN)

$(BIN): $(OBJ_FILES)
	@echo "$(STRING1)"
	@$(CXX) $(CXXFLAGS) $(OBJ_FILES) -o $(BIN)
	@echo "$(CYANN)comp$(NON)..."$(BIN)"...$(GREEN)OK$(NON)"

$(OBJ_DIR):
	mkdir $(OBJ_DIR)

$(OBJ_DIR)/main.cpp: $(SRC_DIR)/main.cpp $(HED)
	@$(CXX) $(CXXFLAGS) -o $@ -c $<
	@echo "$(CYANN)comp$(NON)..."$@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp $(INC_DIR)/%.hpp $(HED)
	@$(CXX) $(CXXFLAGS) -o $@ -c $<
	@echo "$(CYANN)comp$(NON)..."$@

clean:
	@echo "$(STRING2)"
	@rm -rf $(OBJ_DIR)

fclean: clean
	@echo "$(STRING3)"
	@rm -rf $(BIN)

clear:
	@echo "$(STRING2)"
	@rm -rf $(OBJ)
	@echo "$(STRING3)"
	@rm -rf $(BIN)

re: fclean all

run:
	@echo "$(STRING4)"
	$(foreach a, $(TEST_FILES), ./$(BIN) $(RUN) $a;)

print:
	@echo "$(STRING4)"
	$(foreach a, $(TEST_FILES), ./$(BIN) $(PRINT) $a;)

test:
	@$(CXX) $(CXXFLAGS) ./src/test.cpp -o tst
	./tst

inst:
	@echo "$(STRING5)"
	@cp $(BIN) ~/my_bin
